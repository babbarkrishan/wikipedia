package com.babbarkrishan.wikipedia;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.babbarkrishan.wikipedia.bean.WikipediaDTO;
import com.babbarkrishan.wikipedia.exception.WikipediaException;
import com.babbarkrishan.wikipedia.service.ParserService;
import com.babbarkrishan.wikipedia.service.ReaderService;
import com.babbarkrishan.wikipedia.util.StringUtil;



/**
 * @author Krishan Babbar
 * 
 * This program is to find out the answers for given questions from a given paragraph.
 * You are provided with short paragraphs of text from Wikipedia. For each paragraph, there is a set of five questions, which can be answered by reading the paragraph.
 * Answers are of entirely factual nature, e.g. date, number, name, etc. and will be exact substrings of the paragraph. 
 * Answers could either be one word, group of words, or a complete sentence. You are also provided with the answers to each of these questions, 
 * though they are jumbled up, separated by semicolon, and provided in no specific order. 
 * Your task is to identify, which answer corresponds to which question.  
 *
 * Solution:
 * We are parsing paragraphs into separate lines. Then storing question and answers also in separate lines. For this we created a DTO.
 * Then we find lines matching with each question and search answers in those lines considering answer would be the exact substring.
 * 
 */
@SpringBootApplication
public class WikipediaApplication implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(WikipediaApplication.class);
	
	@Autowired
    private ReaderService readerService;
	
	@Autowired
	private ParserService parserService;
	
	@Autowired
	private StringUtil stringUtil;
	
	public static void main(String[] args) {
		SpringApplication.run(WikipediaApplication.class, args);
		try {
			System.in.read();
		} catch (IOException e) {
			logger.error("Reading exception at end.", e);
		}
	}

	@Override
	public void run(String... args) {
		List<String> fileContents;
		try {
			fileContents = readerService.read();
			WikipediaDTO dto = parserService.parse(fileContents);		
			this.showAnswers(dto);
		} catch (WikipediaException e) {
			logger.error("Unable to proceed.", e);
			System.out.println(e.getMessage());
		}		
				
	}
	
	private void showAnswers(WikipediaDTO dto) {		
		long startTime =System.currentTimeMillis();
		
		for(String questionStr: dto.getQuestions()) {
			String answer = stringUtil.getMatchingAnswerStr(questionStr, dto.getParagraphLines(), dto.getAnswers());
			System.out.println(answer);
		}	
		
		long endTime =System.currentTimeMillis();
		long totalTime = endTime - startTime;
		logger.debug("Total Time Taken: " + totalTime);
	}	
}
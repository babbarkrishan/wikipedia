package com.babbarkrishan.wikipedia.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.babbarkrishan.wikipedia.Constants;

/**
 * @author Krishan Babbar
 * 
 * This class is for string utility functions. Mainly it is matching paragraph lines with questions first 
 * then checking which answer best matches with filtered out paragraph lines. 
 *
 */
@Component
public class StringUtil {

	private static final Logger logger = LoggerFactory.getLogger(StringUtil.class);

	@Value("${stop.words.pattern}")
	private String stopWordsPattern;

	public String getMatchingAnswerStr(String question, List<String> pLines, List<String> answers) {
		logger.debug("getMatchingAnswerStr question: {}", question);
		
		//question = this.cleanText(question);
		question = question.substring(0, question.length()-1).trim();
	
		Map<Integer, String> qWordsCount = new TreeMap<Integer, String>(Collections.reverseOrder());
		Set<String> qWords = new HashSet<String>(Arrays.asList(question.split(Constants.WORDS_SEPERATOR)));
		
		// Find out all the paragraph lines which are matching with given questions in descending order, means maximum words found at top.
		for (int i = 0; i < pLines.size(); i++) {
			String pLine = pLines.get(i);
			int count = 0;
			
			for (String qWord: qWords) {
				if (pLine.contains(qWord)) {
					count++;
				}				
			}
			if (count != 0) {
				qWordsCount.put(count, pLine);
			}
		}
		
		// Iterate paragraph lines found above and check with each answer if it is available in paragraph line (In descending order). 
		for(Map.Entry<Integer, String> entry : qWordsCount.entrySet()) {
		    String pLine = entry.getValue();
		    
			for (int i = 0; i < answers.size(); i++) {
				String answer = answers.get(i);
				
				if (pLine.contains(answer)) {
					return answer.trim();
				}
			}
		}
		return null;
	}
	
	public String cleanText(String str) {		
		Pattern stopWords = Pattern.compile(stopWordsPattern, Pattern.CASE_INSENSITIVE);
		Matcher matcher = stopWords.matcher(str);
		String clean = matcher.replaceAll("");
		return clean;
	}
}

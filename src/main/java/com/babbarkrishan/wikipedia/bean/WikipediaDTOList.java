package com.babbarkrishan.wikipedia.bean;

import java.util.List;

public class WikipediaDTOList {
	private List<String> paragraphLines;
	private List<String> questions;
	private List<String> answers;

	public List<String> getParagraphLines() {
		return paragraphLines;
	}
	public void setParagraphLines(List<String> paragraphLines) {
		this.paragraphLines = paragraphLines;
	}
	public List<String> getQuestions() {
		return questions;
	}
	public void setQuestions(List<String> questions) {
		this.questions = questions;
	}
	public List<String> getAnswers() {
		return answers;
	}
	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}
	
	@Override
	public String toString() {
		return "WikipediaDTO [paragraphLines=" + paragraphLines + ", questions=" + questions + ", answers=" + answers
				+ "]";
	}	
}

package com.babbarkrishan.wikipedia.exception;

public class WikipediaException extends Exception {

	public WikipediaException(String string) {
		super(string);
	}
	
}

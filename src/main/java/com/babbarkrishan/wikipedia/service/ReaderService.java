package com.babbarkrishan.wikipedia.service;

import java.util.List;

import com.babbarkrishan.wikipedia.exception.WikipediaException;

public interface ReaderService {

	public List<String> read() throws WikipediaException;
}

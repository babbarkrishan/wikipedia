package com.babbarkrishan.wikipedia.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.babbarkrishan.wikipedia.bean.WikipediaDTO;
import com.babbarkrishan.wikipedia.exception.WikipediaException;


/**
 * @author Krishan Babbar
 * 
 * This will parse the given contents and populate the DTO having paragraph lines, questions and answers
 *
 */
@Service
public class ParserServiceImpl implements ParserService {
	
	private static final Logger logger = LoggerFactory.getLogger(ParserServiceImpl.class);
	
	@Value("${paragraph.line.seperator}")
	private String paragraphLineSeperator;	
	
	@Value("${answers.seperator}")
	private String answersSeperator;
	
	@Value("${questions.count}")
	private int quesCount;
	
	/*@Autowired
	private StringUtil strUtil;*/
	
	@Override
	public WikipediaDTO parse(List<String> fileContents) throws WikipediaException {
		logger.debug("Parsing fileContents: {}", fileContents);
		if (fileContents.size() < (quesCount + 2)) {
			logger.error("Input file does not have required number of lines. Lines count required: {}", (quesCount + 2));
			throw new WikipediaException("Input file does not have required number of lines. Lines count required: " + (quesCount + 2));
		}
		WikipediaDTO dto = new WikipediaDTO();
		
		//String paragraph = strUtil.cleanText(fileContents.get(0));
		String paragraph = fileContents.get(0);
		dto.setParagraphLines(Arrays.asList(paragraph.split(Pattern.quote(paragraphLineSeperator))));
		//this.prepareDictionary(dto);
		
		List<String> questions = new ArrayList<String>();
		for (int i = 1; i <= quesCount; i++) {
			String question = fileContents.get(i).trim();
			//String question = strUtil.cleanText(fileContents.get(i)).trim();
			questions.add(question);
		}
		dto.setQuestions(questions);
		
		String answers = fileContents.get(quesCount+1);
		
		dto.setAnswers(Arrays.asList(answers.split(Pattern.quote(answersSeperator))));
		return dto;
	}
	
	/*private void prepareDictionary(WikipediaDTO dto) {
		Map<String, Set<Integer>> dictionary = new HashMap<String, Set<Integer>>();
		
		for (int i = 0; i < dto.getParagraphLines().size(); i ++) {
			String line = dto.getParagraphLines().get(i);
			List<String> words = Arrays.asList(line.split(Pattern.quote(Constants.WORDS_SEPERATOR)));
			for (String word: words) {
				word = word.toLowerCase();
				if (dictionary.get(word) == null) {
					Set<Integer> lineIndexes = new HashSet<Integer>();
					lineIndexes.add(i);
					dictionary.put(word, lineIndexes);
					continue;
				}
				dictionary.get(word).add(i);
			}
		}
		
		dto.setDictionary(dictionary);		
	}*/

}

package com.babbarkrishan.wikipedia.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.babbarkrishan.wikipedia.exception.WikipediaException;

/**
 * @author Krishan Babbar
 * 
 * This class will read the input file (given in properties file) store each line in ArrayList and return the same.
 *
 */
@Service
public class FileReaderService implements ReaderService {
	
	private static final Logger logger = LoggerFactory.getLogger(FileReaderService.class);
	
	@Value("${inputFile}")
    private String inputFile;
	
	@Override
	public List<String> read() throws WikipediaException {
		List<String> fileContents = Collections.emptyList();
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		try {
			fileContents = new ArrayList<String>();
			fileReader = new FileReader(new File(inputFile));
			bufferedReader = new BufferedReader(fileReader);
			String line;
			while ((line = bufferedReader.readLine()) != null) {				
				if (!line.trim().isEmpty()) {
					fileContents.add(line);
				}
			}
			
		} catch (FileNotFoundException e) {
			logger.error("Unable to read file ", e);
			throw new WikipediaException("Given input file does not exist. Input file name: " + inputFile);
		} catch (IOException e) {
			logger.error("Unable to read file ", e);
			throw new WikipediaException("Unable to read given input file. Please make sure it has read access. Input file name: " + inputFile);
		}
		finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				}
				catch (IOException ex) {
					logger.error("Failed to close file reader.", ex);
				}
			}
		}		
		
		return fileContents;
	}

}

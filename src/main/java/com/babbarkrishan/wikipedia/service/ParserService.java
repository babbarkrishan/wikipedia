package com.babbarkrishan.wikipedia.service;

import java.util.List;

import com.babbarkrishan.wikipedia.bean.WikipediaDTO;
import com.babbarkrishan.wikipedia.exception.WikipediaException;

public interface ParserService {

	WikipediaDTO parse(List<String> fileContents) throws WikipediaException;
}
package com.babbarkrishan.wikipedia;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.babbarkrishan.wikipedia.bean.WikipediaDTO;
import com.babbarkrishan.wikipedia.exception.WikipediaException;
import com.babbarkrishan.wikipedia.service.ParserService;
import com.babbarkrishan.wikipedia.service.ReaderService;
import com.babbarkrishan.wikipedia.util.StringUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WikipediaApplicationTests {

	@Autowired
    private ReaderService readerService;
	
	@Autowired
	private ParserService parserService;
	
	@Autowired
	private StringUtil stringUtil;
	
	private WikipediaDTO dto; 
			
	@Before
	public void init() throws WikipediaException {
		List<String> fileContents = readerService.read();		
		dto = parserService.parse(fileContents);		
	}
	
	@Test
	public void question1Test() {
		String questionStr = "Which Zebras are endangered?";
		String answer = stringUtil.getMatchingAnswerStr(questionStr, dto.getParagraphLines(), dto.getAnswers());
		assertEquals("Grevy's zebra and the mountain zebra", answer);		
	}
	
	@Test
	public void question2Test() {
		String questionStr = "What is the aim of the Quagga Project?";
		String answer = stringUtil.getMatchingAnswerStr(questionStr, dto.getParagraphLines(), dto.getAnswers());
		assertEquals("aims to breed zebras that are phenotypically similar to the quagga", answer);		
	}
	
	@Test
	public void question3Test() {
		String questionStr = "Which animals are some of their closest relatives?";
		String answer = stringUtil.getMatchingAnswerStr(questionStr, dto.getParagraphLines(), dto.getAnswers());
		assertEquals("horses and donkeys", answer);		
	}
	
	@Test
	public void question4Test() {
		String questionStr = "Which are the three species of zebras?";
		String answer = stringUtil.getMatchingAnswerStr(questionStr, dto.getParagraphLines(), dto.getAnswers());
		assertEquals("the plains zebra, the Grevy's zebra and the mountain zebra", answer);		
	}
	
	@Test
	public void question5Test() {
		String questionStr = "Which subgenus do the plains zebra and the mountain zebra belong to?";
		String answer = stringUtil.getMatchingAnswerStr(questionStr, dto.getParagraphLines(), dto.getAnswers());
		assertEquals("subgenus Hippotigris", answer);		
	}

}
